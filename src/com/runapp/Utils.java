package com.runapp;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

/*this class is for some general utilities in the APP*/

public class Utils {

	private static final String CONFIGURATION_NAME = "localConfig";
	public static final String FULLNAME = "fullName";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String LOGGED_EMAIL = "logged";
	public static final String RACE = "Race";
	public static final String USER = "User";
	public static final String LOGGED_FULLNAME = "fullName";
	private static final String SIZE_APPEND = "_size";
	private Context context;

	/**
	 * obtains the total km of a Polilyne.
	 * if the polyline has less than 2 points, it returns "0"
	 * @param line
	 * @return
	 */
	public String getKm(Polyline line){

		List<LatLng> points = line.getPoints();
		if (points == null || points.size() <= 1) {
			return "0";
		}
		Double distance = 0.0;
		// the first previousPoint is the first
		LatLng previousPoint = points.get(0);
		Location locationA = new Location("point A");
		Location locationB = new Location("point B");
		for (LatLng currentPoint : points) {
			locationA.setLatitude(currentPoint.latitude);
			locationA.setLongitude(currentPoint.longitude);
			locationB.setLatitude(previousPoint.latitude);
			locationB.setLongitude(previousPoint.longitude);

			Float distanceAB = locationA.distanceTo(locationB);
			distance += distanceAB;
			// for the next iteration, the current point will be the previous
			// one
			previousPoint = currentPoint;
		}

		return String.valueOf(round(distance,2));
	}

	/**
	 * rounds (instead of truncating) a double to specified number of decimal places.
	 * @param value
	 * @param places
	 * @return
	 */
	@SuppressLint("UseValueOf")
	public Double round(Double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		try{
			Double d = new Double(tmp / factor);
			return d;
		}catch(Exception e){
			return 0.0;
		}
	}

	public void savePreferences(String tag,String text){

		SharedPreferences settings = context.getSharedPreferences(CONFIGURATION_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(tag,text);
		editor.commit();
	}

	//loads the configuration of the Android App using SharedPreferences
	public String getEmailPreferences()
	{    	
		SharedPreferences settings = context.getSharedPreferences(CONFIGURATION_NAME, Context.MODE_PRIVATE);
		String email = settings.getString(LOGGED_EMAIL, "");
		return email;
	}

	public Boolean isPreferences()
	{    	
		SharedPreferences settings = context.getSharedPreferences(CONFIGURATION_NAME, Context.MODE_PRIVATE);
		@SuppressWarnings("unchecked")
		Map<String,Object> objs = (Map<String, Object>) settings.getAll();
		return !objs.isEmpty();

	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public void logout() {

		SharedPreferences settings = context.getSharedPreferences(CONFIGURATION_NAME, Context.MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.clear();
		editor.commit();
	}

	public void logAllOut() {

		SharedPreferences settings = context.getSharedPreferences(CONFIGURATION_NAME, Context.MODE_PRIVATE);
		Editor editor = settings.edit();

		editor.clear();
		editor.commit();
	}

	public Dialog createDialog(Context context,String text,final boolean isRunning)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle("");
		builder.setMessage(text);
		builder.setPositiveButton(R.string.OK, new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
			}
		});
		builder.setNegativeButton(R.string.cancel, new OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
			}
		});



		return builder.create();
	}

	/**
	 * get a long value and returns the difference between that value and now
	 * it returns the difference in the String format hh:mm:ss
	 * @param lastTime
	 * @return
	 */
	public String getRunnedTime(Long lastTime) {

		long now = System.currentTimeMillis();

		long difference = now - lastTime;
		difference = difference/1000;
		int hours = (int) difference / 3600;
		int remainder = (int) difference - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		String timeString = hours + ":" + mins + ":" + secs;

		return timeString;
	}

	/**
	 * Transforms a List<LatLng> readable by a GoogleMap's Polyline to
	 * a List<String[]> that is easier to store by the Datastore
	 * @param points
	 * @return
	 */
	public LinkedList<String[]> transformsListPointsToListArray(List<LatLng> points){
		LinkedList<String[]> pointsStr = new LinkedList<String[]>();
		for(LatLng point : points){
			String[] pointArr = new String[2];
			pointArr[0] = String.valueOf(point.latitude);
			pointArr[1] = String.valueOf(point.longitude);
			pointsStr.add(pointArr);
		}
		return pointsStr;
	}

	/**
	 * Transforms a List<LatLng> readable by a GoogleMap's Polyline to
	 * a List<String[]> that is easier to store by the Datastore
	 * @param points
	 * @return
	 */
	public LinkedList<LatLng> transformsListArraysToListPoints(List<String[]> points){
		LinkedList<LatLng> pointsMap = new LinkedList<LatLng>();
		for(String[] point : points){
			LatLng latlng = new LatLng(Double.parseDouble(point[0]), Double.parseDouble(point[1]));
			pointsMap.add(latlng);
		}
		return pointsMap;
	}

	public boolean saveArray(LinkedList<String> entity, Context mContext) {

		SharedPreferences prefs = mContext.getSharedPreferences(
				CONFIGURATION_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
		String entityName = entity.get(0)+"_"+entity.get(1)+"_"+entity.get(2);
		editor.putInt(entityName + SIZE_APPEND, entity.size());

		for (int i = 1; i < entity.size(); i++)
			editor.putString(entityName + "_" + i, entity.get(i));
		return editor.commit();

	}

	public LinkedList<String> loadArray(String entity, String email, Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		String entityName = entity+"_"+email;
		int size = prefs.getInt(entityName + SIZE_APPEND, 0);
		LinkedList<String> list = new LinkedList<String>();
		for(int i=1;i<size;i++)
			list.add(prefs.getString(entityName + "_" + i, null));
		return list;
	}

	public String getLoggedEmail(Context mContext) {

		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		return prefs.getString(LOGGED_EMAIL, "");
	}
	public String getLoggedFullName(Context mContext) {

		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		return prefs.getString(LOGGED_FULLNAME, "");
	}

	public void setloggedFullName(Context mContext, String fullName2) {
		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(LOGGED_FULLNAME, fullName2);
		editor.commit();
		return;
	}

	public void setLoggedEmail(Context mContext,String email) {
		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(LOGGED_EMAIL, email);
		editor.commit();
		return;

	}

	public LinkedHashMap<String,LinkedList<String>> getRaces(String entity, String email, Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences(CONFIGURATION_NAME, 0);
		String entityName = entity+"_"+email;
		Set<String> subset = prefs.getAll().keySet();
		Iterator<String> it = subset.iterator();
		LinkedList<String> list = new LinkedList<String>();
		LinkedHashMap<String,LinkedList<String>> map = new LinkedHashMap<String,LinkedList<String>>();

		int i = 0;
		while(it.hasNext()){
			String key = it.next();
			if(key.contains(SIZE_APPEND) || prefs.getAll().get(key).equals(email)){
				continue;
			}

			if(key.contains(entityName)){
				String value = (String) prefs.getAll().get(key);
				list.add(value);

				if(list.size()==3){

					@SuppressWarnings("unchecked")
					LinkedList<String> list2 = (LinkedList<String>) list.clone();
					map.put(entityName+i, list2);
					i++;
					list.clear();
				}
			}
		}

		return map;
	}


}
