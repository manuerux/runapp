package com.runapp;

import java.util.LinkedList;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment1 extends Fragment {
	
	private Context context;
	Button buttonLogin;
	String email;
	String password;
	private FragmentManager fragmentManager;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View inflatedView =  inflater.inflate(R.layout.fragment_1, container,false);
		
		TextView registerScreen = (TextView) inflatedView.findViewById(R.id.link_to_register);
		buttonLogin = (Button) inflatedView.findViewById(R.id.btnLogin);
		final EditText emailEditText = (EditText)inflatedView.findViewById(R.id.emailText);
		emailEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		final EditText passEditText = (EditText)inflatedView.findViewById(R.id.passwordText);
		
		//performing the login action
		buttonLogin.setOnClickListener(new View.OnClickListener() {
			
		@Override
			public void onClick(View v) {
				email = emailEditText.getText().toString();
				password = passEditText.getText().toString();
				if(email==null || email.isEmpty()){
					Toast.makeText(context.getApplicationContext(), "El email no puede estar vacío!!!", Toast.LENGTH_SHORT).show();
					return;
				}
				
				Utils utils = new Utils();
				LinkedList<String> userLogged = utils.loadArray("User", email, context);
				
				if(!userLogged.get(2).equals(password)){
					Toast.makeText(context, "Contraseña incorrecta", Toast.LENGTH_LONG).show();
					return;
				}
				utils.setLoggedEmail(context, email);
				if(userLogged.isEmpty()){
					Toast.makeText(context, "NO se ha logueado correcetamente el usuario "+email, Toast.LENGTH_LONG).show();
				}
				else{
					Toast.makeText(context, "Se ha logueado correctamente el usuario "+email, Toast.LENGTH_LONG).show();
				}
				//new GetEntityAsyncTask().execute();
			}
		});
		
		// register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {
 
            public void onClick(View v) {
                // Switching to Register screen
               Intent i = new Intent(context, RegisterActivity.class);
               startActivityForResult(i, 0);
            }
        });
        return inflatedView;
	}
	
	 public class GetEntityAsyncTask extends AsyncTask<Void, Void, Void>{
	    	
	    	@Override
	    	protected void onPreExecute(){}
	    	
	    	@Override
	    	protected Void doInBackground(Void... arg0){
	    		
	    	
	    	
	    		
// 	        CloudBackend backend = new CloudBackend();
//			CloudEntity user = null;
//			CloudQuery cq = new CloudQuery("User");
//		    cq.setFilter(F.eq("email", email));
////		    cq.setFilter(F.eq("password", password));
//		    
//		    List<CloudEntity> possibleUsers=null;
//		    try {
//		    	possibleUsers = backend.list(cq);
//			} catch (IOException e) {
//				Toast.makeText(context, "Ha habido un fallo, por favor vuelva a intarlo mas tarde", Toast.LENGTH_LONG).show();
//				return null;
//			}
//			if(possibleUsers!=null && !possibleUsers.isEmpty()){
//				user = possibleUsers.get(0);
//				Utils utils = new Utils();
//				utils.setContext(context);
//				utils.savePreferences(user);
////				Toast.makeText(context, "Se ha logueado correcetamente el usuario "+email, Toast.LENGTH_LONG).show();
//			}
//			else{
////				Toast.makeText(context, "El email o contraseñas son incorrectas, vuelva a intentarlo", Toast.LENGTH_LONG).show();
//			}
			
	    	return null;
	    	}
	    	@Override
	    	protected void onPostExecute(Void result){
	    		
	    		FragmentTransaction transaction = fragmentManager.beginTransaction();
            	Fragment1_reg fragment1 = new Fragment1_reg();
				fragment1.setContext(context);
				fragment1.setFragmentManager(fragmentManager);
				Fragment fragment = fragment1;
				
				transaction.replace(R.id.fragment1,fragment);
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
	    		
	    	}
	    }

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public void setFragmentManager(FragmentManager fragmentManager) {
		
		this.fragmentManager = fragmentManager;
	}
}
