package com.runapp;

import java.util.LinkedList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Fragment1_reg extends Fragment {
	
	private Context context;
	private FragmentManager fragmentManager;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View inflatedView = inflater.inflate(R.layout.fragment_logged, container,false);
		Button bLogout = (Button)inflatedView.findViewById(R.id.btnLogout);
		Button bLogAllOut = (Button)inflatedView.findViewById(R.id.btnLogAllOut);
		final Utils utils = new Utils();
    	utils.setContext(context);
		String loggedEmail = utils.getLoggedEmail(context);
		String loggedFullName = utils.getLoggedFullName(context);
		LinkedList<String> user = utils.loadArray("User", loggedEmail+"_"+loggedFullName,context);
		
		TextView tvEmail= (TextView)inflatedView.findViewById(R.id.tvLoggedEmail);
		TextView tvFullName = (TextView)inflatedView.findViewById(R.id.tvLoggedFullName);
//		String email = (String)user.get("email");
//		String fullName = (String)user.get("fullName");
		
		tvEmail.setText(user.get(0));
		tvFullName.setText(user.get(1));
		
    	utils.setContext(context);
		bLogout.setOnClickListener(new View.OnClickListener() {
			
            public void onClick(View arg0) {
            	
            	utils.logout();
            	// returning to Fragment 1
            	FragmentTransaction transaction = fragmentManager.beginTransaction();
            	Fragment1 fragment1 = new Fragment1();
				fragment1.setContext(context);
				fragment1.setFragmentManager(fragmentManager);
				Fragment fragment = fragment1;
				
				transaction.replace(R.id.fragment1,fragment);
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
            }
		});
		
		bLogAllOut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				utils.logAllOut();
            	// returning to Fragment 1
            	FragmentTransaction transaction = fragmentManager.beginTransaction();
            	Fragment1 fragment1 = new Fragment1();
				fragment1.setContext(context);
				fragment1.setFragmentManager(fragmentManager);
				Fragment fragment = fragment1;
				
				transaction.replace(R.id.fragment1,fragment);
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
			}
			
		});
		return inflatedView;
	}

	public void setContext(Context context) {
		// TODO Auto-generated method stub
		this.context = context;
	}

	public void setFragmentManager(FragmentManager fragmentManager) {
		// TODO Auto-generated method stub
		
		this.fragmentManager = fragmentManager;
	}
}
