package com.runapp;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends Activity {
	Context context;
	EditText emailEditText;
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.register);
		context = this;
		emailEditText = (EditText) findViewById(R.id.reg_email);
		emailEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		Button registerButton = (Button) findViewById(R.id.btnRegister);

		registerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				EditText fullNameEditText = (EditText) findViewById(R.id.reg_fullname);   	        
				EditText passwordEditText = (EditText) findViewById(R.id.reg_password);

				String fullName = fullNameEditText.getText().toString();
				String email = emailEditText.getText().toString();
				String pass = passwordEditText.getText().toString();

				LinkedList<String> user = new LinkedList<String>();
				Utils utils = new Utils();
				user.add(Utils.USER);
				user.add(email);
				user.add(fullName);
				user.add(pass);
				utils.setLoggedEmail(context,email);
				utils.setloggedFullName(context,fullName);
				utils.saveArray(user,context);
				finish();
			}
		});
	}
}
