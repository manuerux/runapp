package com.runapp;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class RunMapActivity extends FragmentActivity {
	
	GoogleMap googleMap;
	Double latitude=0.00;
	Double longitude=0.00;
	public TextView tvKM;
	private TextView tvTime;
	Long lastTime;
	RunMapActivity rma;
	Context context;
	Boolean isRunning=Boolean.FALSE;
	Polyline line;
	Utils utils = new Utils();
	PolylineOptions options;
	LocationManager lm;
	LocationListener ll;
	String userEmail = "";
	List<LatLng> mapPoints;
	private static final int MINTIME = 30000;
	private static final int MINDISTANCE = 10;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	           
	        setContentView(R.layout.activitymap);
	        context = this;
	         tvKM = (TextView)findViewById(R.id.textView1);
			 tvKM.setText("Dist 0");
			 tvTime = (TextView)findViewById(R.id.textView2);
			 tvTime.setText("Tiempo 0");
			 final Button start = (Button)findViewById(R.id.btnStartRace);
			 final Button stop=(Button)findViewById(R.id.btnStopRace);
			 final Button save=(Button)findViewById(R.id.btnSave);
			 stop.setEnabled(false);
			 save.setEnabled(false);
			 
	    	 utils.setContext(context);
	    	 userEmail = utils.getLoggedEmail(context);
			 
			 SupportMapFragment mMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
			 
			 googleMap = mMapFragment.getMap();
			 

			 start.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					setUpMap();
					stop.setEnabled(true);
					start.setEnabled(false);
					save.setEnabled(false);
					long now = System.currentTimeMillis();
					lastTime = now;
					
					isRunning = !isRunning;
					new RunMapUpdaterAsyncTask().execute();
					
		    		Toast.makeText(context, R.string.servicestarted, Toast.LENGTH_SHORT).show();
		    		
				}
			});
			 
			 stop.setOnClickListener(new View.OnClickListener() {

		            @Override
		            public void onClick(View v) {
		               
		                AlertDialog.Builder builder = new AlertDialog.Builder(context);
		        	    
		        	    builder.setTitle("");
		        	    builder.setMessage(R.string.doyoureallywannastop);
		        	    builder.setPositiveButton(R.string.cancel, new android.content.DialogInterface.OnClickListener() {
		        	        
		        	    	public void onClick(DialogInterface dialog, int which) {
		        	           
		        	            dialog.cancel();
		        	        }
		        	        });
		        	    builder.setNegativeButton(R.string.OK, new android.content.DialogInterface.OnClickListener() {
		        	        
		        	        	public void onClick(DialogInterface dialog, int which) {
		        	        		isRunning = !isRunning;
		        	        		start.setEnabled(true);
		    		                stop.setEnabled(false);
		    		                
		    		                new RunMapUpdaterAsyncTask().execute();
		    		                lm.removeUpdates(ll);
		    		        		lm = null;
		    		        		
		        	            dialog.cancel();
		        	            if(!userEmail.isEmpty())
		        	            	save.setEnabled(true);
		        	    		
		        	            Toast.makeText(context, R.string.servicestop, Toast.LENGTH_SHORT).show();
		        	    		
		        	        }
		        	        });
		        	    builder.create();
		        	    builder.show();
		            }
		        });
			 
			 save.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("SimpleDateFormat")
				@Override
				public void onClick(View v) {
					
					LinkedList<String> race = new LinkedList<String>();
	    	    	Utils utils = new Utils();
	    	    	race.add(Utils.RACE);
	    	    	race.add(userEmail);
	    	    	
	    	    	Date date = new Date();
	    	    	java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd/MM/yyyy_hh:mm");
	    	    	String formattedDate = sdf.format(date);

	    	    	race.add(formattedDate);
	    	    	
	    	    	String distance = tvKM.getText().toString();
	    	    	race.add(distance);
    				String duration = tvTime.getText().toString();
    				race.add(duration);
    				
    				
    				utils.saveArray(race,context);
					save.setEnabled(false);
					
				}
			});

	    }
	
	private void setUpMap(){
			if(googleMap!=null){
				googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				googleMap.setMyLocationEnabled(true);
				
				lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				ll = new MyLocationListener();
				lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINTIME, MINDISTANCE, ll);
				Criteria criteria = new Criteria();
				String provider = lm.getBestProvider(criteria, true);
				
				Location myLocation = lm.getLastKnownLocation(provider);
				if (myLocation != null){
					latitude = myLocation.getLatitude();
					longitude = myLocation.getLongitude();
				}
				LatLng latlng= new LatLng(latitude, longitude);
				
				googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
				//42.430229381851724,8.640557339569101
				//this is for showing the current location on GMap closely
				//the number is between 1 and 21
				googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
				googleMap.addMarker(new MarkerOptions().position(latlng));
				
				options = new PolylineOptions().add(new LatLng(latitude, longitude))
						.width(25)
						.color(Color.BLUE)
						.geodesic(true);
						
				line = googleMap.addPolyline(options);
				
			}
			
		}
	
	
	public class RunMapUpdaterAsyncTask extends AsyncTask<Void, Integer, Void>{
    	int progress;
    	
    	@Override
    	protected void onPreExecute(){}
    	
    	@Override
    	protected Void doInBackground(Void... arg0){

    		runOnUiThread(new Runnable() {
				@Override
    	             public void run() {
					
					if(!isRunning){
						return;
					}
    	        	 //updating the map route
    	        	 options.add(new LatLng(latitude,longitude));
    	        	 line = googleMap.addPolyline(options);
    	        	
    	        	 //updating the km.
    	        	 String km = utils.getKm(line);
    	             tvKM.setText(km);
    	             
    	             //updating time.
    	             String runnedTime = utils.getRunnedTime(lastTime);
    	             tvTime.setText(runnedTime);
    	         }
    	        });
    		
    		
    		return null;
    	}
    	@Override
    	protected void onPostExecute(Void result){}
    }

	
	private class MyLocationListener implements LocationListener{
		@Override
		public void onLocationChanged(Location location) {
			if(location==null){
				return;
			}
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			new RunMapUpdaterAsyncTask().execute();
		}
		@Override
		public void onProviderDisabled(String provider) {}

		@Override
		public void onProviderEnabled(String provider) {}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	}
	
	@Override
	protected void onDestroy() {
		if(lm!=null){
			lm.removeUpdates(ll);
	 		lm = null;
		}
		super.onDestroy();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	}
	
}
