package com.runapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class NewFragment2 extends Fragment {

	private Context context;
	Double latitude=0.00;
	Double longitude=0.00;
	
	TextView tv;
	View inflatedView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		inflatedView = inflater.inflate(R.layout.fragment_2, container,false);
	
		 Button b = (Button)inflatedView.findViewById(R.id.buttonNewActivity);
		 if(b == null){
			 return inflatedView;
		 }
		 b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				Intent i = new Intent(context, RunMapActivity.class);
				startActivity(i);
			}
		});
		 return inflatedView;
	}
	
	public void setContext(Context context) {
		this.context = context;
	}
	
}
