package com.runapp;

import java.util.ArrayList;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyAdapter extends ArrayAdapter<Node>{

	public MyAdapter(Fragment context) {
         super(context.getActivity(), R.layout.myitem);
         this.context = context.getActivity();
     }
	
	public MyAdapter(Fragment context,ArrayList<Node> l) {
		super(context.getActivity(), R.layout.myitem);
        this.context = context.getActivity();
        this.list = l;
	}

	private Activity context;
	private ArrayList<Node> list;
	int vista;
	
	@Override
	public int getCount() {
		
		return list.size();
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	   LayoutInflater inflater = context.getLayoutInflater();
		
	   View item = inflater.inflate(R.layout.myitem, null);
	   TextView listTitle = (TextView)item.findViewById(R.id.listTitle);
	   listTitle.setText(list.get(position).mDuration);

	   TextView listDate = (TextView)item.findViewById(R.id.listDate);
	   listDate.setText(list.get(position).mKm);
	   TextView listKM = (TextView)item.findViewById(R.id.listKM);
	   listKM.setText(list.get(position).mDate);

	   return(item);
	}

	public Activity getContext() {
		return context;
	}

	public void setContext(Activity context) {
		this.context = context;
	}

}
