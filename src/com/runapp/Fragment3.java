package com.runapp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment3 extends Fragment {

	private ArrayList<Node> actividades = new ArrayList<Node>();
	private Context context;
	private ListView lstListado;
	Fragment3 contextList;
	Utils utils = new Utils();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 
        View v = inflater.inflate(R.layout.fragment_3, container, false);
        contextList=this;
		utils.setContext(context);
        return v;
	}
	
	@Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        String userEmail = utils.getLoggedEmail(context);
		if(userEmail == null || userEmail.isEmpty()){
			//the user is not logged in
			return ;
		}
        setData(userEmail);
        lstListado = (ListView)getView().findViewById(R.id.LstListado);
        lstListado.setEmptyView(getView().findViewById(R.id.emptyView));
        lstListado.setAdapter(new MyAdapter(contextList,actividades));
    }
	
	
	public void setData(String userEmail){
		actividades.clear();

		LinkedHashMap<String,LinkedList<String>> races = utils.getRaces(Utils.RACE,userEmail,context);
	    
		for(Map.Entry<String,LinkedList<String>> race : races.entrySet()){
			Node myNode = new Node();
			LinkedList<String> raceList = race.getValue();
			for(String raceItem :raceList){
				if(raceItem.contains("/"))
					myNode.mDate = raceItem;
				else if (raceItem.contains("."))
					myNode.mKm = raceItem + "m";
				else
					myNode.mDuration = raceItem;
				
			}
			
			actividades.add(myNode);
		}
		
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
