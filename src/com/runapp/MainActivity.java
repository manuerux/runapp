package com.runapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends FragmentActivity {

	Fragment fragment;
	Button b1,b2,b3;
	Boolean isGooglePlay = true;
	NewFragment2 f2;
	FragmentManager fm;
	Context context = null;
	Intent intentThis;
	Intent refresh;
	Utils utils;
	
//	private Boolean isGooglePlay(){
//		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
//		if(status == ConnectionResult.SUCCESS){
//			return true;
//		}
//		else{
//			((Dialog) GooglePlayServicesUtil.getErrorDialog(status, this, 10)).show();
//			return false;
//		}
//	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
			b1 = (Button) findViewById(R.id.button1);
			b2 = (Button) findViewById(R.id.button2);
			b3 = (Button) findViewById(R.id.button3);
			utils = new Utils();
			context = this;
			utils.setContext(context);
			if(!utils.getLoggedEmail(context).isEmpty()){
				b1.setText(R.string.fragment1logged);
			}
			
			refresh = new Intent(this, MainActivity.class);
			intentThis = this.getIntent();
			
			fm = getSupportFragmentManager();

			FragmentTransaction ft = fm.beginTransaction();
			
			StartFragment myFragment = new StartFragment();
			ft.add(R.id.fragment1, myFragment);
			ft.commit();
			
			b1.setOnClickListener(onClickListener);
			b2.setOnClickListener(onClickListener);
			b3.setOnClickListener(onClickListener);
			
		}
		
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Fragment fragment;
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				
				if(v == b1){
					if(utils.getLoggedEmail(context).isEmpty()){
						Fragment1 fragment1 = new Fragment1();
						fragment1.setContext(context);
						fragment1.setFragmentManager(fragmentManager);
						fragment = fragment1;
					}
					else{
						Fragment1_reg fragmentReg = new Fragment1_reg();
						fragmentReg.setContext(context);
						fragmentReg.setFragmentManager(fragmentManager);
						fragment = fragmentReg;
					}
				} else if(v == b2){
					if(f2!=null){
//						getSupportFragmentManager().popBackStack();
						transaction.replace(R.id.fragment1,f2);
						transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
						transaction.commit();
						return;
					}
					f2 =new NewFragment2();
//					f2.setisGooglePlay(isGooglePlay);
					f2.setContext(context);

					fragment = f2;
				} else if(v == b3){
					Fragment3 fragment3 = new Fragment3();
					fragment3.setContext(context);
					fragment = fragment3;
				} else {
					fragment = new StartFragment();
				}
				
				transaction.replace(R.id.fragment1,fragment);
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
				
			}
		};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
